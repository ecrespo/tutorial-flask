#Se importa render_template para renderizar plantillas html
from flask import render_template
#Se importa la aplicacion
from app import app

#Se define la ruta del url raiz e index
@app.route('/')
@app.route('/index')
def index():
    #Se define el nombre del usuario
    usuario = {'nickname': 'Ernesto'}
    #Se define los posts del blog
    posts = [
        {
            'autor': {'nickname': 'John Doe'},
            'cuerpo': 'Un dia en Edimburgo'
        },
        {
            'autor': {'nickname': 'Jane Doe'},
            'cuerpo': 'Muy buena Civil war!'
        }
    ]
    #Se devuelve index.html con el titulo, usuario y posts
    return render_template("index.html",
                           title='Home',
                           usuario=usuario,
                           posts=posts)
